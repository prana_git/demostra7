// iniciamos GoogleAnalytics
_.ga.init('$GA$');
// declaramos la instancia
this.Prana = this.Prana || {};
// metodos genericos
(function($this) {
    // init local
    $this._init = function() {
        // seteamos el datepicker a todos los campos
        $('.field-wrapper.datepicker').find('input').datepicker({
        dateFormat: 'dd/mm/yy',
        changeYear: true,
        changeMonth: true,
        yearRange: "1950:2014"
        });
        $('.chosen').chosen({
        width: '100%',
        no_results_text: 'Elemento no encontrado',
        allow_single_deselect: true
        });
        // verificamos si existe init por modulo
        if (typeof $this.init === 'function') {
            // iniciamos el modulo
            $this.init();
            // eliminamos el init
            delete $this.init;
        }
        // eliminamos el init local
        delete $this._init;
    };
}(Prana));
// inicializamos
$(Prana._init);