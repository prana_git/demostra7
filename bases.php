<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Rockit 2.0</title>
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/iconmoon.css" rel="stylesheet">
<link href="style.css" rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link href="assets/css/color.css" rel="stylesheet">
<link href="assets/css/widget.css" rel="stylesheet">
<link href="assets/css/responsive.css" rel="stylesheet">
<!-- <link href="assets/css/rtl.css" rel="stylesheet"> Uncomment it if needed! -->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper"> 
 <!-- Header Start -->
<?php include('inc/header.php'); ?>
	<!-- Header End -->  
  
  <!-- Bredcrumb -->
   <div class="px-header-element dm7-kv-bg">
   	<div class="container">
		<div class="row">
			<div class="px-fancy-heading align-center">
				<div class="px-spreater2">
					<div class="divider">
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
					</div>
				</div>
				<h2>Bases y Condiciones</h2>
				
			</div>
		</div>
	</div>
   </div>
  <!-- Bredcrumb -->
  <!-- Main Start -->
  <div id="main">
  	<section>
		<div class="container">
			<div class="row">
				<section class="panel-group px-default modern">
					<div id="accordion">
					  <div class="col-md-12">
						  <div class="px-section-title">
							
						  </div>
						  <div class="bases-container">
				            <li><b>1. Organizador</b>: Tabacos del Paraguay S.A., para su producto de la marca PALERMO, y Animales de Radio, en adelante “ADR” organizan la promoción denominada “Demostrá tu música 6”.</li>
				            <li><b>2. Duración y territorio</b>: El plazo para participar de la promoción será desde el lunes 25 de agosto del 2014 hasta el viernes 26 de setiembre del 2014, esta última, fecha en que vence el plazo para la inscripción de las bandas. La promoción será válida en toda la República del Paraguay y es exclusiva para mayores de 18 años de edad. El viernes 17 de octubre del 2014 se llevará a cabo la batalla de bandas en un concierto en vivo de los 6 (seis) grupos seleccionados ante el público y jurado.</li>
				            <li><b>3. Mecánica de la promoción</b>: Los interesados (mayores de 18 años de edad) en participar del concurso deberán llenar el formulario de inscripción que se encuentra disponible en la página Web www.demostratumusica.com y enviar un tema propio en formato Mp3 (128kbps) más un video del grupo tocando (opcional). No podrán participar bandas que ya hayan grabado un DISCO propio. Tampoco podrán participar los ganadores de las ediciones anteriores de “Demostrá tu Música”, ni grupos que no tengan temas propios. En caso de que uno de los integrantes de los grupos seleccionados para la batalla de bandas, no pueda presentarse, este sólo podrá ser reemplazado por otra persona mayor de 18 (dieciocho) años que no sea integrante de grupos con discos propios. En la página Web www.demostratumusica.com, deben registrar y anotar a la banda, de la rama del ROCK o POP y sus varios estilos, con un máximo de 6 (seis) miembros y llenar todos los datos requeridos en el formulario de inscripción. La participación en la promoción no implica obligación de compra. Solo el nombre y el tema musical de los grupos inscriptos será publicado en la Web, el resto de la información será confidencial, solo los organizadores tendrán acceso a la misma. Un jurado designado por TABACOS DEL PARAGUAY S.A., seleccionará a su criterio y arbitrio exclusivo, las 6 (seis) bandas que participarán del concurso a llevarse a cabo el viernes 17 de octubre de 2014 en el Estacionamiento del Casino de Asunción ubicado en España casi Sacramento, o en cualquier otro lugar que TABACOS DEL PARAGUAY S.A. determine. Los participantes seleccionados serán notificados al aire en el programa de ADR por la radioemisora Rock&Pop (95.5) el día lunes 06 de octubre de 2014. Éstos también serán notificados vía telefónica al número consignado en el formulario de inscripción y vía e-mail y en las redes sociales de Palermo y ADR. En la página Web www.demostratumusica.com se publicará el nombre de los 6 (seis) grupos seleccionados, fotografías, y primer nombre de los participantes (integrantes de cada grupo) a las 18:00hrs del lunes 06 de octubre de 2014.<br/>
				            Las 6 (seis) bandas preseleccionadas de todas las inscripciones deberán presentar ante el jurado y el público 2 (dos) temas el día del evento, el viernes 17 de octubre del 2014 en la “Batalla de Bandas” a las 20hs. Estos serán dos (2) temas propios de la banda.  Ese día se seleccionará 1 (un) grupo ganador. Todos los participantes deberán contar con sus respectivas cédulas de identidad paraguaya vigentes y/o pasaporte.<br/>
				            TABACOS DEL PARAGUAY S.A. y ADR pondrán a disposición de los concursantes 1 (una) batería de dos cuerpos, con pedestales para platillos, sin caja y sin platos (cada participante debe llevar el suyo). También tendrán acceso a 2 (dos) amplificadores de guitarra y uno de bajo. El resto de los instrumentos a utilizarse durante la presentación será responsabilidad de cada banda.<br/>
				            El Jurado, compuesto por personalidades del ambiente musical y Tabacos del Paraguay S.A., será el encargado de seleccionar a la mejor banda.<br/>
				            Las bandas que no queden como ganadoras no tendrán derecho a reclamar el premio, reembolso de gastos, compensación o indemnización alguna por su participación.</li>
				            <li><b>4. Premios</b>:</li>
				            <ul>
				                <li>Habrá 1 (un) grupo ganador que será premiado con:</li>
				                <ol>
				                    <li>Grabación de un disco de 8 (ocho) temas que van a formar parte del “Disco” del grupo. Tabacos del Paraguay S.A. y ADR proveerán a los grupos ganadores de 1000 (mil) CD´s con el Disco terminado grabado en estos, para el uso y distribución libre por los grupos. El arte en la tapa de los Discos será diseñado por la agencia publicitaria “Prana”, y tendrá imagen de la marca “Palermo” y ADR tanto en el CD como en la tapa y contratapa de este. Ninguna otra marca podrá figurar en el arte del Disco. Si los músicos quisieran más copias de las provistas por Tabacos del Paraguay S.A., todo el arte deberá ser mantenido a cabalidad tal cual se realizaron las primeras 1000 (mil) copias. Tabacos del Paraguay S.A. y sus marcas podrán utilizar la imagen y el nombre del grupo para cualquier comunicación por parte de las marcas.</li>
				                    <li>Un Video Clip de uno de los temas del Grupo valorizado en el monto estimado por Tabacos del Paraguay S.A. Que incluye:</li>
				                    <ul>
				                        <li>Idea y guion: La banda ganadora en conjunto con la agencia Prana desarrollará el guion de una de las músicas del grupo que será la que contará con un video clip.</li>
				                        <li>Debiendo contar con presencia de productos y logotipo de la marca PALERMO y/o cualquier otra marca de los productos distribuidos por TABACOS en EL VIDEO, hasta 3 apariciones. Ninguna otra marca podrá figurar en el VIDEO.</li>
				                        <li>Pre-producción y producción: búsqueda de locaciones, locaciones.</li>
				                        <li>Realización: Equipo de última generación HD, estilismo y maquillaje durante todo el rodaje para 6 personas, arte básico, catering, rodaje de 3 jornadas (1 en estudio + 2 exteriores)</li>
				                        <li>Pos-producción: edición y post producción. Se realizarán todas las tomas necesarias según el guion. Incluye a un Director de Fotografía, asistentes, productor y estilistas.<br/>
				                        Las locaciones consideradas serán en Asunción y Gran Asunción (en un radio no mayor a 80 km. de Asunción)</li>
				                    </ul>
				                    <li>Ser grupo de “Tabacos del Paraguay S.A.” por el periodo de un (1) año. Tabacos del Paraguay S.A. tendría exclusividad de los grupos para 2 eventos y/o activaciones por un (1) año con el rubro cigarrillos. La realización o no de los eventos queda a criterio exclusivo de Tabacos del Paraguay S.A., no pudiendo el grupo reclamar indemnización alguna en caso de no realizarse los eventos.</li>
				                    <li>Rotación del tema del grupo en el programa ADR y en la Rock & Pop (a criterio de la radio).</li>
				                </ol>
				                <li>Los premios son personales e intransferibles y en ningún caso podrán ser reclamados por terceros distintos del grupo y/o del participante ganador. Los premios tampoco pueden ser reclamados ni reembolsados si los ganadores no cumplen con el plazo de producción de los discos y del video.</li>
				                <li>La banda tiene hasta 6 (seis) meses desde que se gana el concurso, para grabar el disco, realizar la gráfica del álbum, idea/guion del video con Prana, terminar el disco con el proveedor asignado y filmar el video de uno de los temas con la productora asignada por Tabacos del Paraguay S.A.</li>
				                <li>Tabacos del Paraguay S.A. abonará el canon de Derecho de Autor (APA) y registro de las canciones, a nombre de los integrantes del grupo.</li>
				                <li>En ningún caso TABACOS DEL PARAGUAY S.A. se hará cargo de los gastos en que deba incurrir el grupo y/o cada participante para el proceso de producción, grabación, diseño u otro gasto que no esté estipulado en estas bases y condiciones.</li>
				            </ul>
				            <li><b>5. Documentos y otros</b>: En caso que algún miembro de la banda preseleccionada no cuente con documentos o no cumpla con los requisitos establecidos o no se pueda presentar a la presentación ante el jurado, sólo podrá ser reemplazado por una persona mayor de 18 (diez y ocho) años que no sea integrante de grupos con discos propios, ganador de alguna de las ediciones anteriores de “Demostrá tu música”, o participante de esta promoción como integrante de otro grupo.  Podrán hacerse hasta dos cambios. La exclusión de cualquier participante no obligará en ningún caso a TABACOS DEL PARAGUAY S.A. a compensar al participante excluido y/o al grupo.</li>
				            <li><b>6. Omisión de requisitos</b>: Si en la inscripción se hubiere omitido uno o más datos y/o no se lo hubiese contemplado en forma clara la banda quedará automáticamente anulada.</li>
				            <li><b>7. Participantes y funcionarios</b>: Sólo podrán participar de la promoción ciudadanos paraguayos, mayores de 18 años. No podrán participar funcionarios y familiares directos (segundo grado de consanguinidad y segundo de afinidad) de Tabacos del Paraguay S.A., Tabacalera del Este S.A., y Gráfica Mercurio S.A., Distribuidora del Paraguay S.A., Bebidas del Paraguay S.A., Paraguay Soccer S.A,  Habacorp SRL. Ganadera Sofía S.A., Aerocentro S.A., Agrocitrus S.A., Sporting Life S.A., Compañía Agrotabacalera de Paraguay S.A., Transporte Multimodal del Paraguay S.A., Cigartrading SRL., y ADR o la Rock & Pop.<br/>
				            TABACOS DEL PARAGUAY S.A. en ningún caso será responsable por los daños y/o accidentes sufridos por los integrantes de la banda y/o por terceros que pudiesen ocurrir durante la realización del concurso.</li>
				            <li><b>8. Autorización de difusión de imagen y cesión de derechos</b>: Los participantes de la promoción autorizan a TABACOS DEL PARAGUAY S.A. y ADR a publicar sus nombres y fotos en los medios masivos de comunicación y ceden todos los derechos y acciones de explotación y difusión de su imagen individual y de la banda y del premio obtenido en los términos y condiciones que el organizador consideren adecuados, aceptando asimismo utilizar camisetas y cualquier otra indumentaria que le fuera proveída por TABACOS DEL PARAGUAY S.A. y ADR.</li>
				            <li><b>9. Situaciones no previstas</b>: Cualquier imprevisto, situación no estipulada o cualquier situación prevista en estas bases y condiciones que requiera una decisión, será resuelta por TABACOS DEL PARAGUAY S.A. y ADR a su sólo arbitrio y sus decisiones serán irrecurribles.</li>
				            <li><b>10. Aceptación</b>: La participación en esta promoción implica el pleno conocimiento y la total aceptación de todo lo establecido en estas bases y condiciones.</li>
				        </div>
					  </div>
					</div>
				</section>
			</div>
		</div>
	</section>
  </div>
  <!-- Main End --> 
  <!-- Footer Start -->
	<?php include('inc/footer.php'); ?>
	<!-- Footer End -->  
</div>
<script src="assets/scripts/jquery.min.js"></script> 
<script src="assets/scripts/modernizr.min.js"></script> 
<script src="assets/scripts/bootstrap.min.js"></script>
<script src="assets/scripts/menu.js"></script>
<!-- Put all Functions in functions.js --> 
<script src="assets/scripts/functions.js"></script>
</body>
</html>