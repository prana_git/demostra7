<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Rockit 2.0</title>
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/iconmoon.css" rel="stylesheet">
<link href="style.css" rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link href="assets/css/color.css" rel="stylesheet">
<link href="assets/css/widget.css" rel="stylesheet">
<link href="assets/css/responsive.css" rel="stylesheet">
<!-- <link href="assets/css/rtl.css" rel="stylesheet"> Uncomment it if needed! -->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper"> 
 <!-- Header Start -->
<?php include('inc/header.php'); ?>
	<!-- Header End -->  
  
  <!-- Bredcrumb -->
   <div class="px-header-element dm7-kv-bg">
   	<div class="container">
		<div class="row">
			<div class="px-fancy-heading align-center">
				<div class="px-spreater2">
					<div class="divider">
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
					</div>
				</div>
				<h2>Preguntas Frecuentes</h2>	
			</div>
		</div>
	</div>
   </div>
  <!-- Bredcrumb -->
  <!-- Main Start -->
  <div id="main">
  	<section>
		<div class="container">
			<div class="row">
				<section class="panel-group px-default modern">
					<div id="accordion">
					  <div class="col-md-12">
						  <div class="px-section-title">
							<h3>Preguntas Frecuentes – Demostrá Tu Música 7</h3>
						  </div>
						  <div class="bases-wrapper">
					        <h1></h1>
					        <div class="bases-container">
					            <li><b>¿Qué es el Demostrá Tu Música 6?</b><br>
					                Es la Sexta Edición del concurso de bandas de música del estilo Rock y afines organizado por Tabacos del Paraguay S.A. para su marca Palermo en conjunto con ADR (Animales de Radio).</li>
					            <li><b>¿Cuántos ganadores habrá?</b><br>
					                1 (UNA) será la banda seleccionada como ganadora.</li>
					            <li><b>¿Cuál es el premio?</b><br>
					                Como todos los años, el premio para la banda ganadora es la grabación del primer disco de la banda con 1000 copias reproducidas. Y además este año 2014, Palermo lleva a tu banda a otro nivel con la producción del video de una de las canciones.</li>
					            <li><b>¿Cómo inscribo a mi banda?:</b><br>
					                Antes que nada, tenés que ser mayor de 18 años, así como TODOS los demás integrantes de tu banda.<br>
					                Después tenés que entrar a www.demostratumusica.com y seguir el instructivo de inscripción.<br>
					                Es muy importante que tengas todo lo requerido ANTES de empezar la inscripción, así no tenés que volver a repetir todo el proceso de nuevo.
					            </li>
					            <li><b>¿Cuándo puedo inscribir a mi banda?</b><br>
					                El día lunes 25 de Agosto estará habilitada la página web para iniciar las inscripciones y el cierre de inscripción es el día viernes 26 de setiembre hasta las 23:59h vía www.demostratumusica.com.<br>
					                Desde el sábado 27 de setiembre a las 00:00 ya ninguna banda será admitida.
					            </li>
					            <li><b>¿Cómo sé si mi banda está inscripta?</b><br>
					                Después de haber completado todo el formulario de inscripción en <a href="screen!inscribe">http://www.demostratumusica.com/screen!inscribe</a>, el sistema tarda 24h en validar todos los datos y actualizarlo. ¡Después podés ingresar a <a href="screen!gallery">http://www.demostratumusica.com/screen!gallery</a> para ver tu banda!</li>
					            <li><b>¿Cuándo se comunicará las bandas seleccionadas?</b><br>
					                Las bandas seleccionadas para la Batalla serán comunicadas el día lunes 6 de octubre, a partir de las 16:00h en el programa Animales de Radio en la Rock And Pop. Además, comunicaremos por redes sociales y contactaremos con cada una de las elegidas.</li>
					            <li><b>¿Cómo se eligen las bandas?</b><br>
					                A través de funcionarios de Tabacos del Paraguay S.A., ADR y un jurado.</li>
					            <li><b>¿Cuándo es el evento/Batalla?</b><br>
					                La Batalla de bandas es el día viernes 17 de octubre de 2014 en el local definido por Tabacos del Paraguay S.A.</li>
					            <li><b>¿Cuándo se sabrá cuál es la banda ganadora?</b><br>
					                El mismo día de la batalla, viernes 17 de octubre, en el evento se comunicará la banda ganadora a la cual Palermo le grabará su primer disco y el video de UNA de sus músicas.</li>
					        </div>
					    </div>
					  </div>
					</div>
				</section>
			</div>
		</div>
	</section>
  </div>
  <!-- Main End --> 
  <!-- Footer Start -->
	<?php include('inc/footer.php'); ?>
	<!-- Footer End -->  
</div>
<script src="assets/scripts/jquery.min.js"></script> 
<script src="assets/scripts/modernizr.min.js"></script> 
<script src="assets/scripts/bootstrap.min.js"></script>
<script src="assets/scripts/menu.js"></script>
<!-- Put all Functions in functions.js --> 
<script src="assets/scripts/functions.js"></script>
</body>
</html>