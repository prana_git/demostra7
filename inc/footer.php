<footer id="footer">
		<div class="container">
			<div class="row">
				<aside class="col-md-12" style="text-align: center">
					<img src="assets/images/dm7-logo-metal.png" alt="">
				</aside>
			</div>
		</div>
		<section id="copyright">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="social-media">
							<ul>
							    <li class="col-md-6">
								    <a href="#" data-original="facebook">
								    <i class="icon-facebook9"></i>
									<span>Facebook</span>
								    </a>
							    </li>
							    <li class="col-md-6">
								    <a href="#" data-original="twitter">
								    <i class="icon-twitter6"></i>
									<span>Twitter</span>
								    </a>
							    </li>
							</ul>
						</div>
						<p>FUMAR PRODUCE CÁNCER Y ENFERMEDADES RESPIRATORIAS. Lo advierte el Ministerio de Salud Pública y Bienestar Social.</p>
					</div>
				</div>
			</div>
		</section> 
	</footer>