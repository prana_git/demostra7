<?php
    namespace py\com\prana\php\dbo;

    use PDO;
    use PDOException;

    /**
     * Database
     *
     * @author Gilberto Ramos <ramos.amarilla@gmail.com>
     * @author Hermann D. Schimpf <hschimpf@hds-solutions.net>
     */
    final class DB {
        /**
         * Conexiones abiertas
         * @var array
         */
        private static $connections = Array();

        /**
         * Retorna una conexion a la base de datos
         * @param string $trxName Nombre de la transaccion
         * @return DB Conexion a la DB
         */
        public static function getConnection($trxName = null) {
            // si no hay transaccion usamos una clave generica (null como clave no guarda en el array)
            $trxName = $trxName === null ? 'NULL' : $trxName;
            // verificamos si la conexion existe
            if (!array_key_exists($trxName, self::$connections)) {
                // creamos la conexion
                self::$connections[$trxName] = self::newConnection();
                // verificamos si se especifico transaccion
                if ($trxName !== 'NULL')
                    // iniciamos la transaccion en la conexion
                    self::$connections[$trxName]->beginTransaction();
            }
            // retornamos la conexion
            return self::$connections[$trxName];
        }

        /**
         * Finaliza la transaccion
         * @param type $trxName Nombre de la transaccion
         * @return boolean
         */
        public static function commitTransaction($trxName) {
            // si no hay transaccion usamos una clave generica (null como clave no guarda en el array)
            $trxName = $trxName === null ? 'NULL' : $trxName;
            // verificmaos si existe la transaccion
            if (!array_key_exists($trxName, self::$connections))
                // retornamos false
                return false;
            // confirmamos la transaccion
            $result = self::$connections[$trxName]->commit();
            // iniciamos una nueva transaccion
            self::$connections[$trxName]->beginTransaction();
            // retornamos el resultado
            return $result;
        }

        /**
         * Cancela una transaccion en curso
         * @param type $trxName Nombre de la transaccion
         * @return boolean
         */
        public static function rollbackTransaction($trxName) {
            // si no hay transaccion usamos una clave generica (null como clave no guarda en el array)
            $trxName = $trxName === null ? 'NULL' : $trxName;
            // verificmaos si existe la transaccion
            if (!array_key_exists($trxName, self::$connections))
                // retornamos false
                return false;
            // confirmamos la transaccion
            $result = self::$connections[$trxName]->rollBack();
            // eliminamos la transaccion
            self::$connections[$trxName] = null;
            // eliminamos la clave
            unset(self::$connections[$trxName]);
            // retornamos el resultado
            return $result;
        }

        /**
         *
         * @param string $user
         * @param string $password
         * @param string $dbname
         * @param string $host
         */
        private static function newConnection($user = 'db157136_webs', $password = 'web@Prana!2014', $dbname = 'db157136_webs', $host = null) {
            // ruta de conexion a la base de datos
            $dsn = 'mysql:dbname=' . $dbname . ';host=' . ($host ? $host : (isset($_ENV['DATABASE_SERVER']) ? $_ENV['DATABASE_SERVER'] : 'external-db.s157136.gridserver.com'));
            try {
                // creamos la conexion
                $dbc = new PDO($dsn, $user, $password);
                // seteamos los errores a excepcion
                $dbc->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                // seteamos los caracteres a UTF8
                $dbc->query("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");
                // retornamos la conexion
                return $dbc;
            } catch (PDOException $e) {
                die('Fallo conexion: ' . $e->getMessage());
            }
        }
    }