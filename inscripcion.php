<?php
    // habilitamos los errores
   /* error_reporting(E_ALL);
    ini_set('display_errors', true);
    // TimeZone
    date_default_timezone_set('America/Asuncion');
   // cargamos las clases
    include_once 'inc/py/com/prana/php/dbo/DB.php';
    // inicializamos session
    session_start();
    use \py\com\prana\php\dbo\DB;*/
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Rockit 2.0</title>
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/iconmoon.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/css/jquery.chosen.css"/>
<link href="style.css" rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link href="assets/css/color.css" rel="stylesheet">
<link href="assets/css/widget.css" rel="stylesheet">
<link href="assets/css/inscribe.css" rel="stylesheet">
<link href="assets/css/responsive.css" rel="stylesheet">

<!-- <link href="assets/css/rtl.css" rel="stylesheet"> Uncomment it if needed! -->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="wrapper"> 
  <!-- Header Start -->
<?php include('inc/header.php'); ?>
	<!-- Header End -->  
  <!-- Main Start -->
  <div id="main">
	<section class="bg-form dm7-kv-bg">
		<div class="container">
			<div class="row">
				<section class="px-form plain">
					<div class="col-md-12">
						<div class="px-fancy-heading align-center">
							<div class="px-spreater2">
								<div class="divider">
									<span></span>
									<span></span>
									<span></span>
									<span></span>
									<span></span>
									<span></span>
									<span></span>
								</div>
							</div>
							<h2>LLEVA TU GRUPO A OTRO NIVEL</h2>
							<h4>PARTICIPÁ POR LA GRABACIÓN DE TU PRIMER VIDEO Y DISCO</h4>	
							<p>ANTES DE COMPLETAR AL FORMULARIO DE INSCRIPCIÓN, TE RECOMENDAMOS FAMILIARIZARTE CON LOS 3 PASOS CON QUE CUENTA EL PROCESO. ESTO TE AYUDARÁ A TENER A MANO LOS DATOS QUE SON NECESARIOS PARA LA INSCRIPCIÓN DE TU BANDA</p>
						</div>
						<form>
							<h2>1- DATOS DE LA BANDA</h2>
							<label class="error-alerta">
								<i class="icon-music6"></i>
								<input type="text" required="" class=" " placeholder="Nombre de la Banda" name="banda-nombre">
								<span class="error-msg">Este campo es obligatorio</span>
							</label>
							<label>
								<i class="icon-location6"></i>
								<input type="text" required="" class=" " placeholder="Direccion de Ensayo" name="banda-direccion">
							</label>
							<label>
								<i class="icon-location6"></i>
								<input type="text" required="" class=" " placeholder="Ciudad" name="banda-ciudad">
							</label>
							<label>
								<i class="icon-music6"></i>
								<input type="text" required="" class=" " placeholder="Edad de la Banda" name="banda-edad">
							</label>
							<label class="input-full">
								<i class="icon-music6"></i>
								<input type="text" required="" class=" " placeholder="Estilos de la Banda" name="banda-estilos">
							</label>
							<label class="input-full">
								<i class="icon-music6"></i>
								<input type="text" required="" class=" " placeholder="Influencias musicales" name="banda-influencias">
							</label>
							<label class="textaera-sec">
								<i class="icon-text"></i>
								<textarea placeholder="¿Por qué deberían ganar el concurso?"></textarea>
							</label>

							<h3>Datos del Manager</h3>
							<label>
								<i class="icon-user9"></i>
								<input type="text" required="" class=" " placeholder="Numero de Documento" name="manager-ci">
							</label>
							<label>
								<i class="icon-user9"></i>
								<input type="text" required="" class=" " placeholder="Nombre y Apellido" name="manager-nombre">
							</label>
							<label>
								<i class="icon-user9"></i>
								<input type="text" required="" class=" " placeholder="Fecha de Nacimiento" name="manager-nacimiento">
							</label>
							<label>
								<i class="icon-mail6"></i>
								<input type="email" required="" class=" " placeholder="Email" name="manager-email">
							</label>

							<label class="input-full">
								<i class="icon-mobile4"></i>
								<input type="text" required="" class=" " placeholder="Telefono" name="manager-telefono">
							</label>


							<h2>2- INTEGRANTES</h2>

							<h4>Integrante 1:</h4>
							<label>
								<i class="icon-user9"></i>
								<input type="text" required="" class=" " placeholder="Numero de Documento" name="integrante-ci">
							</label>
							<label>
								<i class="icon-user9"></i>
								<input type="text" required="" class=" " placeholder="Nombre y Apellido" name="integrante-nombre">
							</label>
							<label>
								<i class="icon-user9"></i>
								<input type="text" required="" class=" " placeholder="Fecha de Nacimiento" name="integrante-nacimiento">
							</label>
							<label>
								<i class="icon-mail6"></i>
								<input type="email" required="" class=" " placeholder="Email" name="integrante-email">
							</label>
							<label>
								<i class="icon-mobile4"></i>
								<input type="text" required="" class=" " placeholder="Telefono" name="integrante-telefono">
							</label>
							<label>
								<i class="icon-microphone5"></i>
								<input type="text" required="" class=" " placeholder="Instrumentos" name="integrante-instrumentos">
							</label>
							<label>
								<i class="icon-facebook-square"></i>
								<input type="text" required="" class=" " placeholder="Facebook" name="integrante-fb">
							</label>
							<label>
								<i class="icon-twitter-square"></i>
								<input type="text" required="" class=" " placeholder="Twitter" name="integrante-tw">
							</label>

							<label class="boton-subir-archivo pull-right">
							    Foto del Integrante <input type="file" style="display: none;">
							</label>

							<hr>

							<label class="submit-sec">
								<input type="submit" value="Añadir Integrante">
							</label>

							<h2>3- MULTIMEDIA DE LA BANDA</h2>
							<div class="col-md-4">
								<figure class="banda-album-detalle">
				                	<img src="assets/extra-images/thestrokes.jpg" alt=""/>
				                </figure>
							</div>
							<div class="col-md-8">
								<label class="input-full">
									<i class="icon-file-photo-o"></i>
									<input type="text" required="" class=" " placeholder="Logo de la Banda" name="logo-banda">
									<p class="ayuda-texto">Recomendamos subir una imagen cuadrada, minima de 340 pixeles</p>
								</label>

								<label class="input-full">
									<i class="icon-file-sound-o"></i>
									<input type="text" required="" class=" " placeholder="Tema de la Banda (Formato .mp3, tamaño maximo 10MB)" name="musica">
								</label>
								<label class="input-full">
									<i class="icon-music6"></i>
									<input type="text" required="" class=" " placeholder="Nombre del Tema" name="tema">
								</label>
								<label class="input-full">
									<i class="icon-youtube"></i>
									<input type="text" required="" class=" " placeholder="Video de la Banda Youtube" name="yt">
								</label>
								<label class="input-full">
									<i class="icon-file-photo-o"></i>
									<input type="text" required="" class=" " placeholder="Subi mas fotos de la banda" name="fotos">
								</label>
							</div>
							

							<h3 class="redes-title">Redes Sociales</h3>
							<label class="input-full">
								<i class="icon-facebook-square"></i>
								<input type="text" required="" class=" " placeholder="Facebook" name="fotos">
							</label>
							<label class="input-full">
								<i class="icon-twitter-square"></i>
								<input type="text" required="" class=" " placeholder="Twitter" name="fotos">
							</label>
							<label class="input-full">
								<i class="icon-youtube"></i>
								<input type="text" required="" class=" " placeholder="Canal Youtube" name="fotos">
							</label>
							<label class="submit-sec">
								<input type="submit" value="Inscribir">
							</label>
						</form>
					</div>
				</section>
			</div>
		</div>
	</section>
  </div>
  <!-- Main End --> 
  <!-- Footer Start -->
	<?php include('inc/footer.php'); ?>
	<!-- Footer End -->  
</div>
<script src="assets/scripts/jquery.min.js"></script> 
<script src="assets/scripts/modernizr.min.js"></script> 
<script src="assets/scripts/bootstrap.min.js"></script>
<script src="assets/scripts/menu.js"></script>
<!-- Put all Functions in functions.js --> 
<script src="assets/scripts/functions.js"></script>
<script src="assets/scripts/jquery.chosen.min.js"></script>
<script src="assets/scripts/main.js"></script>
</body>
</html>