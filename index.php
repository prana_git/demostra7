<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Rockit 2.0</title>
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/bootstrap-theme.min.css" rel="stylesheet">
<link href="assets/css/iconmoon.css" rel="stylesheet">
<link href="style.css" rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link href="assets/css/color.css" rel="stylesheet">
<link href="assets/css/widget.css" rel="stylesheet">
<link href="assets/css/browser-detect.css" rel="stylesheet">
<link href="assets/css/responsive.css" rel="stylesheet">
<!-- <link href="assets/css/rtl.css" rel="stylesheet"> Uncomment it if needed! -->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- fb  -->

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.6&appId=130504600357401";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>



<div class="wrapper"> 
	 <!-- Header Start -->
<?php include('inc/header.php'); ?>
	<!-- Header End -->  
	<!-- Banner Start -->
	<section class="px-banner banner-home">
		<div class="banner-holder">
			<div class="flexslider px-loading">
			<div class="loader"><img src="assets/images/squares.gif" alt="" /></div>
				<ul class="slides">
					<li><img src="assets/extra-images/slider-img-1.jpg" alt="" />
						<!--<div class="banner-mask"><img src="assets/images/slider-border.png" alt="" /></div>-->
						<div class="caption">
							<!--<h2>Wow Awards<br/>
								VIP Upgrades for South America!</h2>
							<p>The start of the GNR South American run is only days away. Do you have your tickets yet?? If you do, don't miss your chance to NOW upgrade your experience to VIP! Upgrades are now available for purchase! </p>-->
							<img src="assets/extra-images/dm7logo-slogan.png" alt="">
							<img src="assets/extra-images/lamusicadejamarcas.png" alt="">
							<a href="inscripcion.php" class="px-readmore">INSCRÍBANSE!</a>
							
						</div>
					</li>
					<li><img src="assets/extra-images/slider-img-2.jpg" alt="" />
						<!--<div class="banner-mask"><img src="assets/images/slider-border.png" alt="" /></div>-->
						<div class="caption">
							<!--<h2>Wow Awards<br/>
								VIP Upgrades for South America!</h2>
							<p>The start of the GNR South American run is only days away. Do you have your tickets yet?? If you do, don't miss your chance to NOW upgrade your experience to VIP! Upgrades are now available for purchase! </p>-->
							<img src="assets/extra-images/dm7logodorado-slogan.png" alt="">
							<img src="assets/extra-images/lamusicadejamarcas.png" alt="">
							<a href="inscripcion.php" class="px-readmore">INSCRÍBANSE!</a>
							
						</div>
					</li>
					<li><img src="assets/extra-images/slider-img-3.jpg" alt="" />
						<!--<div class="banner-mask"><img src="assets/images/slider-border.png" alt="" /></div>-->
						<div class="caption">
							<!--<h2>Wow Awards<br/>
								VIP Upgrades for South America!</h2>
							<p>The start of the GNR South American run is only days away. Do you have your tickets yet?? If you do, don't miss your chance to NOW upgrade your experience to VIP! Upgrades are now available for purchase! </p>-->
							<img src="assets/extra-images/dm7logobob-slogan.png" alt="">
							<img src="assets/extra-images/lamusicadejamarcas.png" alt="">
							<a href="inscripcion.php" class="px-readmore">INSCRÍBANSE!</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</section>
	<!-- Banner End --> 
	<!-- Main Start -->
	<div id="main">
		<section style="display:none">
			<div class="container">
				<div class="row">
					<section class="px-band-news">
						<div class="col-md-12">
							<div class="main-heading">
								<h2>Noticias</h2>
							</div>
							<div class="holder">
								<article class="col-md-12">
									<div class="px-holder"> <img src="assets/extra-images/news-img-1.jpg" alt="" />
										<div class="px-info"> <span class="px-author"><em>by</em> David Moleena</span>
											<h3><a href="#">Snow Patrol's new album fallen Empires</a></h3>
											<span class="date"><i class="icon-circle-thin"></i>December 23, 2014</span><span class="px-catagory"><i class="icon-circle-thin"></i>Music, Fun, Entertainment</span> <a href="#" class="btn-next"><img src="assets/images/arrow.png" alt="" /></a> </div>
									</div>
								</article>
								<article class="col-md-12">
									<div class="px-holder"> <img src="assets/extra-images/news-img-2.jpg" alt="" />
										<div class="px-info"> <span class="px-author"><em>by</em> David Moleena</span>
											<h3><a href="#">Snow Patrol's new album fallen Empires</a></h3>
											<span class="date"><i class="icon-circle-thin"></i>December 23, 2014</span><span class="px-catagory"><i class="icon-circle-thin"></i>Music, Fun, Entertainment</span> <a href="#" class="btn-next"><img src="assets/images/arrow.png" alt="" /></a> </div>
									</div>
								</article>
								<article class="col-md-12">
									<div class="px-holder"> <img src="assets/extra-images/news-img-3.jpg" alt="" />
										<div class="px-info"> <span class="px-author"><em>by</em> David Moleena</span>
											<h3><a href="#">Snow Patrol's new album fallen Empires</a></h3>
											<span class="date"><i class="icon-circle-thin"></i>December 23, 2014</span><span class="px-catagory"><i class="icon-circle-thin"></i>Music, Fun, Entertainment</span> <a href="#" class="btn-next"><img src="assets/images/arrow.png" alt="" /></a> </div>
									</div>
								</article>
							</div>
						</div>
					</section>
				</div>
			</div>
		</section>
		
		<section>
			<div class="container">
				<div class="row">
					<div class="main-heading">
						<h2>Seguinos</h2>
					</div>
					<div class="col-md-12">
						<div style="margin: 0 auto; width:500px; text-align:center; margin-bottom:40px">
							<div class="fb-page" style="margin: 0 auto; width:500px; text-align:center" data-href="https://www.facebook.com/Demostr%C3%A1-tu-M%C3%BAsica-521240394726909/?fref=ts" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="upcoming-event">
			<div class="container">
				<div class="row">
					<section class="px-event list">
					<div class="row">
					   	<div class="main-heading">
							<h2>Calendario</h2>
						</div>
						<article class="col-md-4">
							<div class="event-box box">
								<div class="event-date box">
									<div class="date-inner">
										<strong>06</strong>
										<span>JUN</span>
									</div>
								</div>
								<div class="event-inner box">
									<div class="text">
										
										<h4><a href="#">Conferencia de prensa en Rockero</a></h4>
										<ul>
											<li>
												<i class="icon-calendar5"></i>
												<p>
													Lunes 06 de Junio de 2016<br>
													
												</p>
											</li>
											<li>
												<i class="icon-location6"></i>
												<p>
													Rockero<br>
													Centro, Asuncion
												</p>
											</li>
										</ul>
										<!--<a href="#" class="ticket-btn">Tickets</a>-->
									</div>
								</div>
							</div>
						</article>
						<article class="col-md-4">
							<div class="event-box box">
								<div class="event-date box">
									<div class="date-inner">
										<strong>13</strong>
										<span>JUN</span>
									</div>
								</div>
								<div class="event-inner box">
									<div class="text">
										<!--<span>Live Performence</span>-->
										<h4><a href="#">Lanzamiento Web Inscripciones</a></h4>
										<ul>
											<li>
												<i class="icon-calendar5"></i>
												<p>
													Lunes 13 de Junio de 2016<br>
													
												</p>
											</li>
											<li>
												<i class="icon-location6"></i>
												<p>
													&nbsp; <!-- Colocar este espaciador para que no se rompa la grilla del template -->
													<br>
													&nbsp;
												</p>
											</li>
										</ul>
										<!--<a href="#" class="ticket-btn">Tickets</a>-->
									</div>
								</div>
							</div>
						</article>
						<article class="col-md-4">
							<div class="event-box box">
								<div class="event-date box">
									<div class="date-inner">
										<strong>13</strong>
										<span>JUL</span>
									</div>
								</div>
								<div class="event-inner box">
									<div class="text">
										
										<h4><a href="#">Cierre de Inscripciones</a></h4>
										<ul>
											<li>
												<i class="icon-calendar5"></i>
												<p>
													Miercoles 13 de Julio de 2016<br>
													
												</p>
											</li>
											<li>
												<i class="icon-location6"></i>
												<p>
													&nbsp; <!-- Colocar este espaciador para que no se rompa la grilla del template -->
													<br>
													&nbsp;
												</p>
											</li>
										</ul>
										<!--<a href="#" class="booked-btn">Booked</a>-->
									</div>
								</div>
							</div>
						</article>
						
					</div>
				</section>
				</div>
			</div>
		</section>
		
		<section class="px-team-bg dm7-kv-bg mg-b-0">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="main-heading">
							<h2>ÚLTIMAS BANDAS INSCRIPTAS</h2>
						</div>
						<div class="px-team team-grid">
							<div class="row">
								<article class="col-md-3">
									<a href="#">
										<figure class="effect-selena"> <img src="assets/extra-images/sttepenwolf.jpg" alt=""/></figure>
									</a>
									<div class="px-text"> <a href="#">Sttepenwolf</a> <span>Classic Rock</span> </div>
								</article>
								<article class="col-md-3">
									<div class="px-holder">
										<a href="#">
											<figure class="effect-selena"><img src="assets/extra-images/redhotchilipeppers.jpg" alt="" /></figure>
										</a>
										<div class="px-text"><a href="#">Red Hot Chili Peppers</a> <span>Alternative Rock</span> </div>
									</div>
								</article>
								<article class="col-md-3">
									<div class="px-holder">
										<a href="#">
											<figure class="effect-selena"><img src="assets/extra-images/thestrokes.jpg" alt="" />
											</figure>
										</a>
										<div class="px-text"><a href="#">The Strokes</a> <span>Rock</span> </div>
									</div>
								</article>
								<article class="col-md-3">
									<a href="#">
										<figure class="effect-selena"> <img src="assets/extra-images/sttepenwolf.jpg" alt=""/></figure>
									</a>
									<div class="px-text"> <a href="#">Sttepenwolf</a> <span>Classic Rock</span> </div>
								</article>
							</div>					
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="px-grally fancy-grally">
			<div class="col-md-12">
				<div class="row">
					<article class="col-md-2">
						<figure class="effect-selena"><img src="assets/extra-images/grall-img-1.jpg" alt=""/>
							<figcaption>
								<div class="px-text">
									<i class="icon-camera5"></i>
									<a href="#">8Photos</a>
									<span>Amazing Music Concert <em>in Dubai</em></span>
								</div>
							</figcaption>
						</figure>
					</article>
					<article class="col-md-2">
					    <figure class="effect-selena"><img src="assets/extra-images/grall-img-2.jpg" alt=""/>
					    	<figcaption>
								<div class="px-text">
									<i class="icon-camera5"></i>
									<a href="#">8Photos</a>
									<span>Amazing Music Concert <em>in Dubai</em></span>
								</div>
							</figcaption>
					    </figure>
					</article>
					<article class="col-md-2">
						<figure class="effect-selena"><img src="assets/extra-images/grall-img-3.jpg" alt=""/>
						 <figcaption>
								<div class="px-text">
									<i class="icon-camera5"></i>
									<a href="#">8Photos</a>
									<span>Amazing Music Concert <em>in Dubai</em></span>
								</div>
							</figcaption>
						</figure>
					</article>
					<article class="col-md-2">
						<figure class="effect-selena"><img src="assets/extra-images/grall-img-4.jpg" alt=""/>
							<figcaption>
								<div class="px-text">
									<i class="icon-camera5"></i>
									<a href="#">8Photos</a>
									<span>Amazing Music Concert <em>in Dubai</em></span>
								</div>
							</figcaption>
						</figure>
					</article>
					<article class="col-md-2">
						<figure class="effect-selena"><img src="assets/extra-images/grall-img-5.jpg" alt=""/>
							<figcaption>
								<div class="px-text">
									<i class="icon-camera5"></i>
									<a href="#">8Photos</a>
									<span>Amazing Music Concert <em>in Dubai</em></span>
								</div>
							</figcaption>
						</figure>
					</article>
					<article class="col-md-2">
						<figure class="effect-selena"><img src="assets/extra-images/grall-img-6.jpg" alt=""/>
							<figcaption>
								<div class="px-text">
									<i class="icon-camera5"></i>
									<a href="#">8Photos</a>
									<span>Amazing Music Concert <em>in Dubai</em></span>
								</div>
							</figcaption>
						</figure>
					</article>
					<article class="col-md-2">
					    <figure class="effect-selena"><img src="assets/extra-images/grall-img-7.jpg" alt=""/>
					    	<figcaption>
								<div class="px-text">
									<i class="icon-camera5"></i>
									<a href="#">8Photos</a>
									<span>Amazing Music Concert <em>in Dubai</em></span>
								</div>
							</figcaption>
					    </figure>
					</article>
					<article class="col-md-2">
						<figure class="effect-selena"><img src="assets/extra-images/grall-img-8.jpg" alt=""/>
							<figcaption>
								<div class="px-text">
									<i class="icon-camera5"></i>
									<a href="#">8Photos</a>
									<span>Amazing Music Concert <em>in Dubai</em></span>
								</div>
							</figcaption>
						</figure>
					</article>
					<article class="col-md-2">
						<figure class="effect-selena"><img src="assets/extra-images/grall-img-9.jpg" alt=""/>
							<figcaption>
								<div class="px-text">
									<i class="icon-camera5"></i>
									<a href="#">8Photos</a>
									<span>Amazing Music Concert <em>in Dubai</em></span>
								</div>
							</figcaption>
						</figure>
					</article>
					<article class="col-md-2">
						<figure class="effect-selena"><img src="assets/extra-images/grall-img-10.jpg" alt=""/>
							<figcaption>
								<div class="px-text">
									<i class="icon-camera5"></i>
									<a href="#">8Photos</a>
									<span>Amazing Music Concert <em>in Dubai</em></span>
								</div>
							</figcaption>
						</figure>
					</article>
				</div>
			</div>
		</section>
		
	</div>
	<!-- Main End --> 
	 <!-- Footer Start -->
	<?php include('inc/footer.php'); ?>
	<!-- Footer End -->  
</div>
<script src="assets/scripts/jquery.min.js"></script> 
<script src="assets/scripts/modernizr.min.js"></script> 
<script src="assets/scripts/bootstrap.min.js"></script> 
<script src="assets/scripts/browser-detect.js"></script> 
<script src="assets/scripts/selectFx.js"></script> 
<script src="assets/scripts/menu.js"></script>
<script src="assets/scripts/jquery.flexslider.js"></script> 
<script src="assets/scripts/jquery.countdown.js"></script> 
<script src="assets/scripts/jquery.matchHeight.js"></script>
<script src="assets/scripts/slick-min.js"></script>
<script src="assets/scripts/slick.js"></script> 
<!-- Put all Functions in functions.js --> 
<script src="assets/scripts/functions.js"></script>
</body>
</html>