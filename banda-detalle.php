<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Rockit 2.0</title>
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/iconmoon.css" rel="stylesheet">
<link href="style.css" rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link href="assets/css/color.css" rel="stylesheet">
<link href="assets/css/widget.css" rel="stylesheet">
<link href="assets/css/browser-detect.css" rel="stylesheet">
<link href="assets/css/responsive.css" rel="stylesheet">
<link href="assets/css/jplayer.blue.monday.min.css" rel="stylesheet">
<!-- <link href="assets/css/rtl.css" rel="stylesheet"> Uncomment it if needed! -->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper"> 
  <!-- Header Start -->
	<?php include('inc/header.php'); ?>
	<!-- Header End --> 
  <section>
	<div class="albumbanner">
      <section class="px-player">
        <div class="container">
          <div class="row">
            <div class="col-md-4">
            	<figure class="banda-album-detalle">
                	<img src="assets/extra-images/thestrokes.jpg" alt=""/>
                </figure>
            </div>
            <div class="col-md-8">
				<div class="text">
					<!--<a href="#" class="tracks-btn">10 tracks</a>-->
					<h2 class="titulo-banda-detalle">The Strokes</h2>
					<ul class="post-options">
						<li><i class="icon-calendar6"></i>Año: <span>2012</span></li>
						<li>
							<i class="icon-folder-open-o"></i>
							Genero: 
							<a href="#">Rock,</a>
							<a href="#">POP,</a>
							<a href="#">Night,</a>
							<a href="#">Remix</a>
						</li>
					</ul>
				</div>
              	<div id="jquery_jplayer_1" class="jp-jplayer"></div>
              	<div id="jp_container_1" class="jp-audio" role="application" aria-label="media player">
                <div class="jp-type-playlist">
                  <div class="jp-gui jp-interface">
                    <div class="jp-controls">
                      <button class="jp-previous" tabindex="0">previous</button>
                      <button class="jp-play" tabindex="0">play</button>
                      <button class="jp-next" tabindex="0">next</button>
                    </div>
                    <div class="jp-progress">
                      <div class="jp-seek-bar">
                        <div class="jp-play-bar"></div>
                      </div>
                    </div>
                    <div class="jp-volume-controls">
                      <button class="jp-mute" role="button" tabindex="0">mute</button>
                      <div class="jp-volume-bar">
                        <div class="jp-volume-bar-value"></div>
                      </div>
                    </div>
                  </div>
                  <!--<div class="jp-playlist no2">
                    <ul>
                      <li>&nbsp;</li>
                    </ul>
                  </div>-->
                  <div class="jp-no-solution"> <span>Update Required</span> To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>. </div>
                </div>
              </div>
			  	<div class="social-icons pull-right">
					<a href="#"><i class="icon-facebook2"></i></a>
					<a href="#"><i class="icon-twitter2"></i></a>
				</div>
				<!--
				<div class="social-sharing">
					<ul>
						<li><a href="#"><i class="icon-export"></i>Share</a></li>
						<li class="likes-btn"><a href="#"><i class="icon-heart11"></i></a></li>
					</ul>
				</div>-->
            </div>
          </div>
        </div>
      </section>
    </div>
  </section> 
  <!-- Main Start -->
  <div id="main">
	<section>
		<div class="container">
			<div class="row">
				<section class="album-detail">
					<div class="detail-holder">
						<div class="px-section-title">
							<div class="col-md-12">
								<h3>¿Por qué deberían ganar el concurso?</h3>
							</div>
						</div>
						<div class="rich-editor-text">
							<div class="col-md-12">
								<p>Arrives with a serious over. Snow Patrol's new album, Fallen Empires, arrives with a serious reputation attached. best The Irish/Scottish five piece have sold Fallen Empires, arrives with a serious over. Snow Patrol's new album, Fallen Empires, arrives with a serious reputation attached. The Irish/Scottish five piece have sold Fallen Empires, arrives with a serious over.Snow Patrol's new album, Fallen Empires, arrives with a serious reputation attached. </p>
								<p>The Irish/Scottish five piece have sold Fallen Empires, arrives with a serious over. Snow Patrol's new album, Fallen Empires, arrives with a serious reputation attached. The Irish/Scottish five piece have sold Fallen Empires. Fallen Empires, arrives with a serious reputation attached. best The Irish/Scottish five piece have sold Fallen Empires The Irish/Scottish five piece have sold Fallen Empires, arrives with a serious over. Snow Patrol's new album, Fallen Empires, arrives.</p>
							</div>
						</div>
						<div class="px-team grid">
							<div class="px-section-title">
								<div class="col-md-12">
									<h3>MIEMBROS</h3>
								</div>
							</div>
							<article class="col-md-4">
								<figure>
									<a href="#"><img src="assets/extra-images/team1.jpg" alt="#" /></a>
								</figure>
								<div class="text">
									<h4><a href="#">James Warson</a></h4>
									<div class="info-sec">
										<span>Lead Singer</span>
									</div>
								</div>
							</article>
							<article class="col-md-4">
								<figure>
									<a href="#"><img src="assets/extra-images/team2.jpg" alt="#" /></a>
								</figure>
								<div class="text">
									<h4><a href="#">James Warson</a></h4>
									<div class="info-sec">
										<span>Lead Singer</span>
									</div>
								</div>
							</article>
							<article class="col-md-4">
								<figure>
									<a href="#"><img src="assets/extra-images/team3.jpg" alt="#" /></a>
								</figure>
								<div class="text">
									<h4><a href="#">James Warson</a></h4>
									<div class="info-sec">
										<span>Lead Singer</span>
									</div>
								</div>
							</article>
						</div>
						<div class="video-sec">
							<div class="px-section-title">
								<div class="col-md-12">
									<h3>Video</h3>
								</div>
							</div>
							<div class="col-md-12">
								<iframe src="https://player.vimeo.com/video/25924530?portrait=0" width="100%" height="492" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
	<section>
		
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12">
								<div class="px-section-title">
									<div class="col-md-12">
										<h3>Fotos</h3>
									</div>
								</div>
								<div class="gallery-slider">
									<ul class="px-slider slider-for">
									   <li>
									        <img src="assets/extra-images/gall-slider-img1.jpg" alt=""/>
									   		<div class="px-captions"><span>Sell high-quality apparel with zero upfront costs and zero risk. We ship directly to buyers, you keep the profit.</span></div>
									   </li>
									   <li>
									        <img src="assets/extra-images/gall-slider-img1.jpg" alt=""/>
									   		<div class="px-captions"><span>Sell high-quality apparel with zero upfront costs and zero risk. We ship directly to buyers, you keep the profit.</span></div>
									   </li>
									   <li>
									        <img src="assets/extra-images/gall-slider-img1.jpg" alt=""/>
									   		<div class="px-captions"><span>Sell high-quality apparel with zero upfront costs and zero risk. We ship directly to buyers, you keep the profit.</span></div>
									   </li>
									   <li>
									        <img src="assets/extra-images/gall-slider-img1.jpg" alt=""/>
									   		<div class="px-captions"><span>Sell high-quality apparel with zero upfront costs and zero risk. We ship directly to buyers, you keep the profit.</span></div>
									   </li>
									   <li>
									        <img src="assets/extra-images/gall-slider-img1.jpg" alt=""/>
									   		<div class="px-captions"><span>Sell high-quality apparel with zero upfront costs and zero risk. We ship directly to buyers, you keep the profit.</span></div>
									   </li>
									   <li>
									        <img src="assets/extra-images/gall-slider-img1.jpg" alt=""/>
									   		<div class="px-captions"><span>Sell high-quality apparel with zero upfront costs and zero risk. We ship directly to buyers, you keep the profit.</span></div>
									   </li>
									   <li>
									        <img src="assets/extra-images/gall-slider-img1.jpg" alt=""/>
									   		<div class="px-captions"><span>Sell high-quality apparel with zero upfront costs and zero risk. We ship directly to buyers, you keep the profit.</span></div>
									   </li>
									   <li>
									        <img src="assets/extra-images/gall-slider-img1.jpg" alt=""/>
									   		<div class="px-captions"><span>Sell high-quality apparel with zero upfront costs and zero risk. We ship directly to buyers, you keep the profit.</span></div>
									   </li>
									   <li>
									        <img src="assets/extra-images/gall-slider-img1.jpg" alt=""/>
									   		<div class="px-captions"><span>Sell high-quality apparel with zero upfront costs and zero risk. We ship directly to buyers, you keep the profit.</span></div>
									   </li>
									   <li>
									        <img src="assets/extra-images/gall-slider-img1.jpg" alt=""/>
									   		<div class="px-captions"><span>Sell high-quality apparel with zero upfront costs and zero risk. We ship directly to buyers, you keep the profit.</span></div>
									   </li>
									   <li>
									        <img src="assets/extra-images/gall-slider-img1.jpg" alt=""/>
									   		<div class="px-captions"><span>Sell high-quality apparel with zero upfront costs and zero risk. We ship directly to buyers, you keep the profit.</span></div>
									   </li>
									   <li>
									        <img src="assets/extra-images/gall-slider-img1.jpg" alt=""/>
									   		<div class="px-captions"><span>Sell high-quality apparel with zero upfront costs and zero risk. We ship directly to buyers, you keep the profit.</span></div>
									   </li>
									</ul>
									<ul class="px-thumbnail slider-nav">
									   <li><img src="assets/extra-images/thumbnail-img1.jpg" alt=""/></li>
									   <li><img src="assets/extra-images/thumbnail-img2.jpg" alt=""/></li>
									   <li><img src="assets/extra-images/thumbnail-img3.jpg" alt=""/></li>
									   <li><img src="assets/extra-images/thumbnail-img4.jpg" alt=""/></li>
									   <li><img src="assets/extra-images/thumbnail-img5.jpg" alt=""/></li>
									   <li><img src="assets/extra-images/thumbnail-img6.jpg" alt=""/></li>
									   <li><img src="assets/extra-images/thumbnail-img1.jpg" alt=""/></li>
									   <li><img src="assets/extra-images/thumbnail-img2.jpg" alt=""/></li>
									   <li><img src="assets/extra-images/thumbnail-img3.jpg" alt=""/></li>
									   <li><img src="assets/extra-images/thumbnail-img4.jpg" alt=""/></li>
									   <li><img src="assets/extra-images/thumbnail-img5.jpg" alt=""/></li>
									   <li><img src="assets/extra-images/thumbnail-img6.jpg" alt=""/></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
  </div>
  <!-- Main End --> 
   <!-- Footer Start -->
	<?php include('inc/footer.php'); ?>
	<!-- Footer End --> 
</div>
<script src="assets/scripts/jquery.min.js"></script> 
<script src="assets/scripts/modernizr.min.js"></script> 
<script src="assets/scripts/bootstrap.min.js"></script> 
<script src="assets/scripts/browser-detect.js"></script>
<script src="assets/scripts/menu.js"></script>
<script defer src="assets/scripts/jquery.flexslider.js"></script> 
<script defer src="assets/scripts/jquery.countdown.js"></script> 
<script defer src="assets/scripts/jquery.matchHeight.js"></script> 
<script defer src="assets/scripts/slick-min.js"></script>
<script defer src="assets/scripts/slick.js"></script> 
<script type="text/javascript" src="assets/scripts/jquery.jplayer.min.js"></script> 
<script type="text/javascript" src="assets/scripts/jplayer.playlist.min.js"></script>


<!-- Put all Functions in functions.js --> 
<script src="assets/scripts/functions.js"></script>
</body>
</html>