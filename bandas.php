<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Rockit 2.0</title>
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/bootstrap-theme.min.css" rel="stylesheet">
<link href="assets/css/iconmoon.css" rel="stylesheet">
<link href="style.css" rel="stylesheet">
<link href="assets/css/menu.css" rel="stylesheet">
<link href="assets/css/color.css" rel="stylesheet">
<link href="assets/css/widget.css" rel="stylesheet">
<link href="assets/css/browser-detect.css" rel="stylesheet">
<link href="assets/css/responsive.css" rel="stylesheet">
<!-- <link href="assets/css/rtl.css" rel="stylesheet"> Uncomment it if needed! -->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper"> 
	<!-- Header Start -->
	<?php include('inc/header.php'); ?>
	<!-- Header End --> 
	<!-- Main Start -->
	<div id="main">
		<section class="px-team-bg dm7-kv-bg mg-b-0">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="main-heading">
							<h2>BANDAS INSCRIPTAS</h2>
						</div>
						<div class="px-team team-grid">
							<div class="row">
								<article class="col-md-4">
									<a href="#">
										<figure class="effect-selena"> <img src="assets/extra-images/sttepenwolf.jpg" alt=""/></figure>
									</a>
									<div class="px-text"> <a href="#">Sttepenwolf</a> <span>Classic Rock</span> </div>
								</article>
								<article class="col-md-4">
									<div class="px-holder">
										<a href="#">
											<figure class="effect-selena"><img src="assets/extra-images/redhotchilipeppers.jpg" alt="" /></figure>
										</a>
										<div class="px-text"><a href="#">Red Hot Chili Peppers</a> <span>Alternative Rock</span> </div>
									</div>
								</article>
								<article class="col-md-4">
									<div class="px-holder">
										<a href="#">
											<figure class="effect-selena"><img src="assets/extra-images/thestrokes.jpg" alt="" />
											</figure>
										</a>
										<div class="px-text"><a href="#">The Strokes</a> <span>Rock</span> </div>
									</div>
								</article>
							</div>
							<div class="row">
								<article class="col-md-4">
									<a href="#">
										<figure class="effect-selena"> <img src="assets/extra-images/sttepenwolf.jpg" alt=""/></figure>
									</a>
									<div class="px-text"> <a href="#">Sttepenwolf</a> <span>Classic Rock</span> </div>
								</article>
								<article class="col-md-4">
									<div class="px-holder">
										<a href="#">
											<figure class="effect-selena"><img src="assets/extra-images/redhotchilipeppers.jpg" alt="" /></figure>
										</a>
										<div class="px-text"><a href="#">Red Hot Chili Peppers</a> <span>Alternative Rock</span> </div>
									</div>
								</article>
								<article class="col-md-4">
									<div class="px-holder">
										<a href="#">
											<figure class="effect-selena"><img src="assets/extra-images/thestrokes.jpg" alt="" />
											</figure>
										</a>
										<div class="px-text"><a href="#">The Strokes</a> <span>Rock</span> </div>
									</div>
								</article>
							</div>							
						</div>
					</div>
				</div>
			</div>
		</section>
		
		
		
		
		
	</div>
	<!-- Main End --> 
	 <!-- Footer Start -->
	<?php include('inc/footer.php'); ?>
	<!-- Footer End --> 
</div>
<script src="assets/scripts/jquery.min.js"></script> 
<script src="assets/scripts/modernizr.min.js"></script> 
<script src="assets/scripts/bootstrap.min.js"></script> 
<script src="assets/scripts/browser-detect.js"></script> 
<script src="assets/scripts/selectFx.js"></script> 
<script src="assets/scripts/menu.js"></script>
<script src="assets/scripts/jquery.flexslider.js"></script> 
<script src="assets/scripts/jquery.countdown.js"></script> 
<script src="assets/scripts/jquery.matchHeight.js"></script>
<script src="assets/scripts/slick-min.js"></script>
<script src="assets/scripts/slick.js"></script> 
<!-- Put all Functions in functions.js --> 
<script src="assets/scripts/functions.js"></script>
</body>
</html>